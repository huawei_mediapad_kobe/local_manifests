# Local Manifests #
Just grab the manifest and sync to get device sources
### LineageOS 16.0 ###

```bash

# Grab Local Manifest
curl -o .repo/local_manifests/local_manifests.xml https://gitlab.com/huawei_mediapad_kobe/local_manifests/-/raw/main/local_manifest.xml --create-dirs

# Sync
repo sync -j$(nproc --all) --force-sync
```
